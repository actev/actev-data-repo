	# ActEV-Data-Repo

This GIT Repo is the data distribution machanism for the ActEV
evaluation.  The repo presently consists of a collection of corpora
(plural for corpus) and partition defintion files to be used for
evaluations.  Future additions will include source annotations and
donated data/annotations.

The repo contains textual data but not the large-sized corpora
(videos, etc.).  For these large corpora, the repo contains the
necessary information to download a given corpus giving the user
control of the downloaded content.  The utilities will download data
from HTTP and/or AWS buckets.  See below for installation instuctions.

Section 1 describes the installation and system requirements.  Section
2 below describes how to download corpora.  Section 3 describes the
partitions dir3edctory that describe subset of the data.

1.0 Installation

This GIT repo includes several scripts in the 'scripts' directory.
Presently, the software required includes: Python3 and UNIX SH shell.
Both most be available via your shell's PATH variable.

The following are requirments for using the utilities:

- For download of MEVA data from AWS, either the AWS CLI
  (https://aws.amazon.com/cli/) or s3cmd are required and included in
  your shell's PATH variable.  The AWS CLI is the prefered download
  method.  To use the AWS CLI, you'll need to: 
  - Download the CLI (brew/apt-get)
  - Make a free AWS account
  - Make a set of "Access keys (access key ID and secret access key)"
    - log in to the console (https://aws.amazon.com/console/).  On the pulldown menu for
      your name, select "My security creditials" 
  - Configure the AWS CLI with the command: 'aws configure'. 
  
- Python packages boto3, jsonschema and pandas.  Package boto3 is
  require only if the s3cmd utility is used for AWS downloads.

2.0 Corpora Management

The 'corpora' directory contains a directory named for each 'corpus'.
For example, the 'MEVA' directory contains released data from the MEVA
corpus.  The repo contains a minimal set of files to manage downloading a corpus.

   - MANIFEST -> A CSV file describing each file within the corpus including its 
     	      	 size, md5, URL, etc.  Most datasets require some form of
		 access credentials (e.g. user/password) so the manifest alone
		 will not give you access to the data.
   - credentials.json -> A JSON file containing the access credentials for the 
                 corpus.  This file is managed by the corpora maintainance script
		 and is initially set to be an unusable default value.

There are three steps to download a corpus after the initial clone of
the repo, update your repo, adding access credentials to the corpus
and then downloading the data.

Step 1: Update the repo

Execute the following command to get the latest files:

   % git pull

Step 2: Add Credentials

Add credentials using the actev-corpora-maint.py script.  You will be
provided, either by NIST directly or through a website, the command to
add credentials to for a corpus.  The command will look something
like:

    % python ./scripts/actev-corpora-maint.py --operation summary --corpus MEVA --add_credential "...<STRING>..."

Step 3: Download the data for the corpus

First get a summary of the corpus to download and make sure you have the
space to download:

    % python scripts/actev-corpora-maint.py --operation summary

Next download the data by executing the command below.  Optionally,
the --corpus argument (--corpus VIRAT-V1) will limit the download to
a specific corpus.

    % python scripts/actev-corpora-maint.py --operation download

3. Partitions

Throughout the life of the ActEV challenges, we will identify subsets
of data that can span multiple corpora.  The 'partitions' directory
contains a sub-directory for each identfied partition (i.e., a
subset).  Consult the README in the partitions directory for further
details.

Optionally, the --partitions argument (--partitions ActEV-Eval-CLI-Validation-Set2) 
will limit the download to a specific partition of a specific corpus.

    % python scripts/actev-corpora-maint.py --operation download --partitions ActEV-Eval-CLI-Validation-Set2  

4. Annotations

The 'annotations' directory contains up-to-date annotations for the
included data sets.  The data may be out of sync with the partitions
annotations because the partition annotations are snapshots in time.

5. Distribution

THIS DATA IS PROVIDED "AS IS" for use in the ActEV Evaluations.  With
regard to this data, NIST MAKES NO EXPRESS OR IMPLIED WARRANTY AS TO
ANY MATTER WHATSOEVER, INCLUDING MERCHANTABILITY, OR FITNESS FOR A
PARTICULAR PURPOSE.

EACH CORPORA CONTAINS A SEPARATE LICENSE STATEMENT.
     
6. Contacts

If you have any questions, please add a comment to the GIT Repo

--------------------------------------------

2019.03.26 - README created by Jonathan Fiscus
2021.03.11 - Added support for MEVA AWS downloads rather than using the NIST server.


