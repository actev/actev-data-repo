#!/usr/bin/env python3

# json_validate.py
# Author(s): Jon Fiscus

# This software was developed by employees of the National Institute of
# Standards and Technology (NIST), an agency of the Federal
# Government. Pursuant to title 17 United States Code Section 105, works
# of NIST employees are not subject to copyright protection in the
# United States and are considered to be in the public
# domain. Permission to freely use, copy, modify, and distribute this
# software and its documentation without fee is hereby granted, provided
# that this notice and disclaimer of warranty appears in all copies.

# THE SOFTWARE IS PROVIDED 'AS IS' WITHOUT ANY WARRANTY OF ANY KIND,
# EITHER EXPRESSED, IMPLIED, OR STATUTORY, INCLUDING, BUT NOT LIMITED
# TO, ANY WARRANTY THAT THE SOFTWARE WILL CONFORM TO SPECIFICATIONS, ANY
# IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
# PURPOSE, AND FREEDOM FROM INFRINGEMENT, AND ANY WARRANTY THAT THE
# DOCUMENTATION WILL CONFORM TO THE SOFTWARE, OR ANY WARRANTY THAT THE
# SOFTWARE WILL BE ERROR FREE. IN NO EVENT SHALL NIST BE LIABLE FOR ANY
# DAMAGES, INCLUDING, BUT NOT LIMITED TO, DIRECT, INDIRECT, SPECIAL OR
# CONSEQUENTIAL DAMAGES, ARISING OUT OF, RESULTING FROM, OR IN ANY WAY
# CONNECTED WITH THIS SOFTWARE, WHETHER OR NOT BASED UPON WARRANTY,
# CONTRACT, TORT, OR OTHERWISE, WHETHER OR NOT INJURY WAS SUSTAINED BY
# PERSONS OR PROPERTY OR OTHERWISE, AND WHETHER OR NOT LOSS WAS
# SUSTAINED FROM, OR AROSE OUT OF THE RESULTS OF, OR USE OF, THE
# SOFTWARE OR SERVICES PROVIDED HEREUNDER.

# Distributions of NIST software should also include copyright and
# licensing statements of any third-party software that are legally
# bundled with the code in compliance with the conditions of those
# licenses.

import argparse
import sys
import os
import json
import jsonschema
from jsonschema import validate

from actev_corpora_maint.utils import *

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="JSON Validation Against Schemas")

    parser.add_argument("--schema", type=str, nargs="?", help='Schema File', 
                       default="")
    parser.add_argument("--json", type=str, nargs="?", help='The JSON File to Validate', 
                       default="")
    parser.add_argument("--verbose", help='Report the finding', default=False, action='store_true')

    args = parser.parse_args()

    if (args.schema == ""):
        err_quit("--schema Required")
    if (args.json == ""):
        err_quit("--json Required")

    schema = load_json(args.schema)
    json = load_json(args.json)

    try:
        validate(json, schema)
        if (args.verbose):
            sys.stdout.write("JSON {}: OK\n".format(args.json))
        sys.exit(0)
    except jsonschema.exceptions.ValidationError as ve:
        if (args.verbose):
            sys.stderr.write("JSON {}: ERROR\n".format(args.json))
            sys.stderr.write(str(ve) + "\n")
        sys.exit(1)
