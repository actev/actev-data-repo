#!/bin/sh


writeHeader(){
    #echo "FileID,FileMD5,FileByteSize,RelativeFilePath,URL,URL_Type,URL_MD5,URL_ByteSize,user,password"
    echo "FileID,FileMD5,FileByteSize,URL_Root,URL_RelativePath,URL_Filename,URL_Type,URL_MD5,URL_ByteSize"
}

setName=$1
### This points to the same location, one on www space, one on the file system
urlRoot=$2
fsRoot=$3
md5Prog=$4

writeFileRecord(){
    filename=$1
    fileid=`basename $filename`
    relPath=`dirname $filename|sed "s:^$setName/::" | sed "s:^$setName$:.:"`
    size=`ls -l $fsRoot/$filename | awk '{print $5}'`
    md5=`cat $fsRoot/$filename | $md5Prog | awk '{print $1}'`
    echo $fileid,$md5,$size,$urlRoot/$setName,$relPath,$fileid,file,$md5,$size #===== $fsRoot $setName $filename
}

writeHeader
for f in `(cd $fsRoot; find $setName -type f | sort | grep -v MANIFEST | egrep -v '~$')` ; do
    writeFileRecord $f 
done





