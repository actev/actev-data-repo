import pandas as pd
import subprocess
import os

from actev_corpora_maint.utils import *


class Partition(object):
    ### 5BComputed locations of the top level dirs
    dir_root = ""
    dir_partition = ""

    ### Variables for the specific data set
    set_name = ""
    set_partition_dir = ""
    set_file_index = ""
    credentials = None
    valid = False
    message = ""
    
    def __init__(self, dir_root, set_name):
        # self.radius is an instance variable
        self.dir_root = dir_root
        self.set_name = set_name
        self.files = []
        if os.path.isfile(Partition.get_file_index(dir_root, set_name)):
            self.dir_partitions = Partition.get_partitions_dir(dir_root)
            self.set_partition_dir = Partition.get_partitions_dir(dir_root) + "/" + set_name
            self.set_file_index = Partition.get_file_index(dir_root, set_name)

            ### Determine if the set is valid
            if (not file_exists(self.set_file_index)):
                self.message = "No index-file.json file found for set {}. Expected {}".format(self.set_name,self.set_file_index)
            else:
                ### Load the manifest
                self.files = Partition.get_files(dir_root, self.set_file_index)
                self.valid = True

    @staticmethod
    def get_partitions_dir(root):
        return(root + "/partitions")

    @staticmethod
    def get_credentials_dir(root):
        return(root + "/credentials")

    @staticmethod
    def get_file_index(root, partition_set):
        return(Partition.get_partitions_dir(root) + "/" + partition_set + "/" + "file-index.json")

    @staticmethod
    def get_files(root, file_index):
        with open(file_index) as f:
            file_index_content = json.load(f)
        return list(file_index_content.keys())

    def is_valid(self):
        return(self.valid)

    def get_message(self):
        return(self.message)
            
