import os
import json
import jsonschema
import errno
import subprocess



def err_quit(msg, exit_status=1):
    print("[Error] {}".format(msg))
    exit(exit_status)

def warn(msg):
    print("[Warning] {}".format(msg))

def load_json(json_fn):
    try:
        with open(json_fn, 'r') as json_f:
            return json.load(json_f)
    except IOError as ioerr:
        err_quit("{}. Aborting!".format(ioerr))

def parse_json(json_str):
    try:
        return(json.loads(json_str))
    except ValueError:  # includes simplejson.decoder.JSONDecodeError
        err_quit("Decoding JSON has failed for string {}".format(json_str))

def mkdir_p(path):
    try:
        os.makedirs(path)
    except OSError as exc:
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else:
            err_quit("{}. Aborting!".format(exc))

def dir_exists(path):
    return(os.path.isdir(path))

def file_exists(path):
    return(os.path.isfile(path))

def file_size(path):
    return(os.path.getsize(path))

def file_copy(fro, to):
    try:
        out = subprocess.check_output(['cp', fro, to])
    except subprocess.CalledProcessError as e:
        error_quit("5Ccp {} {} failed".format(fro, to))

def touch_file(path):
    try:
        out = subprocess.check_output(['touch', path])
    except subprocess.CalledProcessError as e:
        error_quit("Touch of File " + path +  " failed")

def load_json(path):
    with open(path) as json_file:  
        try:
            return(json.load(json_file))
        except Exception as e:
            print(e)
            err_quit("Failed to open and read JSON {}".format(path))

def write_backup(path):
    id = 1
    while (file_exists(path + "." + "{}".format(id))):
        id = id +1
    file_copy(path, path + "." + "{}".format(id))

def write_json(path, data):
    with open(path, 'w') as outfile:
        try:
            json.dump(data, outfile, indent=4)
        except Exception as e:
            err_quit("Write of json to {} failed {}".format(path, e))

def delete_file(path):
    if os.path.exists(path):
        os.remove(path)
    else:
        warn("Delete of " + path + " requested but it does note exist")
