import pandas as pd
import subprocess
import os
import re

from actev_corpora_maint.utils import *


class Corpus(object):
    ### 5BComputed locations of the top level dirs
    dir_root = ""
    dir_corpora = ""
    ### Utilities
    transfer_tool = None
    aws_transfer_tool = None

    ### Variables for the specific data set
    set_name = ""
    set_corpus_dir = ""
    set_manifest_file = ""
    set_credentials_file = ""
    manifest = None
    credentials = None
    valid = False
    message = ""
    
    def __init__(self, dir_root, set_name, transfer_tool, aws_transfer_tool):
        # self.radius is an instance variable
        self.dir_root = dir_root
        self.set_name = set_name
        self.transfer_tool = transfer_tool
        self.aws_transfer_tool = aws_transfer_tool
        if os.path.isfile(Corpus.get_corpus_manifest(dir_root, set_name)):
            self.dir_corpora = Corpus.get_corpora_dir(dir_root)
            self.set_corpus_dir = Corpus.get_corpora_dir(dir_root) + "/" + set_name
            self.set_credentials_file = Corpus.get_corpora_dir(dir_root) + "/" + set_name + "/credentials.json"
            self.set_manifest_file = Corpus.get_corpus_manifest(dir_root, set_name)

            ### Determine if the set is valid
            if (not file_exists(self.set_manifest_file)):
                self.message = "No Manifest file found for set {}. Expected {}".format(self.set_name,self.set_manifest_file)
            elif (not file_exists(self.set_credentials_file)):
                self.message = "No Credentials file found for set {}. Expected {}".format(self.set_name, self.set_credentials_file)
            else:
                ### Load the manifest
                self.manifest = pd.read_csv(self.set_manifest_file, dtype=object)
                for col in ['FileID', 'FileMD5', 'FileByteSize', 'URL_Root', 'URL_RelativePath', 'URL_Filename', 'URL_Type', 'URL_MD5', 'URL_ByteSize']:
                    if (not col in self.manifest.keys()):
                        err_quit("Manifest of set {} does not contain column {}.  Please 'git pull' to update the file.".format(set_name, col))
                self.manifest.FileByteSize = self.manifest.FileByteSize.astype(int)

                ### Load the credentials
                self.credentials = load_json(self.set_credentials_file)
                self.valid = True
                self.review_manifest()

    def review_manifest(self):
        file_present = []
        local_file = []
        local_file_byte_size = []
        file_credentialed = []
        for index, row in self.manifest.iterrows():
            lfile = "{}/{}/{}".format(self.set_corpus_dir, str(row['URL_RelativePath']), str(row['FileID']))

            ### if the file exists but is invalid, we need to do something.  It may get deleted if it's garbage
            if (file_exists(lfile)):
                fs = file_size(lfile)
                if (not fs == row['FileByteSize']):               
                    warn("Deleting file {}.  It is present but the size is {} but should be {}".format(lfile, fs, row['FileByteSize']))
                    delete_file(lfile)            
                    if (file_exists(lfile + ".failed")):
                        delete_file(lfile + ".failed")
            
            ### Is the credential available?
            file_credentialed.append(row['URL_Root'] in self.credentials['urls'])

            if (file_exists(lfile)):
                local_file.append(lfile)
                fs = file_size(lfile)
                local_file_byte_size.append(fs)
                if (not fs == row['FileByteSize']):               
                    warn("File {} present but the size is {} but should be {}".format(lfile, fs, row['FileByteSize']))
                    file_present.append(False)
                else:
                    file_present.append(True)
            else:
                file_present.append(False)
                local_file.append(lfile)
                local_file_byte_size.append(None)

        self.manifest['file_present'] = file_present
        self.manifest['file_credentialed'] = file_credentialed
        self.manifest['local_file'] = local_file
        self.manifest['local_file_byte_size'] = local_file_byte_size

    def get_cred(self, url, attr):
        return(self.credentials['urls'][url][attr])

    def _check_regex(self, regex_list, fullfile):
        if (len(regex_list) == 0):
            return(True)
        for regex in regex_list:
            p = re.compile(regex)
            if (p.match(fullfile)):
                return(True)
        return(False)            
    
    def load_corpus(self, dryrun, partition_files, regex_list):
        for index, row in self.manifest.iterrows():
            if row['URL_Type'] == 'file' and self._check_regex(regex_list, self.set_corpus_dir + "/" + row['URL_RelativePath'] + "/" + row['FileID']):
                if (not row['file_present'] and row['file_credentialed']):
                    if (file_exists(row['local_file'] + ".failed")):
                        warn("Delete " + row['local_file'] + ".failed to attempt re-download")
                    else: 
                        if (self.get_cred(row['URL_Root'], 'type') == "file_store"):
                            if (self.transfer_tool == 'wget'):
                                com='wget --user {} --password "{}" -P "{}" "{}"'.format(self.get_cred(row['URL_Root'], 'user'), self.get_cred(row['URL_Root'], 'password'),
                                                                                      self.set_corpus_dir + "/" + row['URL_RelativePath'], row['URL_Root'])
                            elif (self.transfer_tool == 'curl'):
                                com='curl --user "{}:{}" "{}" --output "{}"'.format(self.get_cred(row['URL_Root'], 'user'),
                                                                                    self.get_cred(row['URL_Root'], 'password'),
                                                                                    row['URL_Root'] + "/" + row['URL_RelativePath'] + "/" + row['URL_Filename'],
                                                                                    self.set_corpus_dir + "/" + row['URL_RelativePath'] + "/" + row['FileID'])
                            else:
                                err_quit("Internal File_store Error")
                        elif (self.get_cred(row['URL_Root'], 'type') == "aws_bucket"):
                            if (self.aws_transfer_tool == 's3cmd'):
                                com='s3cmd get {}/{}/{} {}/{}/{}'.format(row['URL_Root'], row['URL_RelativePath'], row['URL_Filename'],
                                                                         self.set_corpus_dir, row['URL_RelativePath'], row['URL_Filename'])
                            elif (self.aws_transfer_tool == 'aws'):
                                remote = '{}/{}/{}'.format(row['URL_Root'], row['URL_RelativePath'], row['URL_Filename'])
                                if (row['URL_RelativePath'] == '.'):  ### Handle files in the top dir
                                    remote = '{}/{}'.format(row['URL_Root'], row['URL_Filename'])
                                com='aws s3 cp {} {}/{}/{}'.format(remote,self.set_corpus_dir, row['URL_RelativePath'], row['URL_Filename'])
                            else:
                                err_quit("Internal AWS Bucket Error")
                            
                        # print(com)
                        if not dryrun:
                            if (not partition_files) or (partition_files and row['FileID'] in partition_files):
                                print("Downloading {}".format(row['FileID']))
                                mkdir_p(self.set_corpus_dir + "/" + row['URL_RelativePath'])
                                try:
                                    out = subprocess.check_output(com, shell=True)
                                except subprocess.CalledProcessError as e:
                                #print(e.output)
                                    warn("File "+row['local_file']+" exception")
                                    touch_file(row['local_file'] + ".failed")
                                else:
                                ### Did it work?
                                    if (not file_exists(row['local_file'])):
                                        warn("File "+row['local_file']+" not downloaded")
                                    elif (file_size(row['local_file']) != row['FileByteSize']):
                                        warn("File "+row['local_file']+" downloaded but file size is wrong")
                                        touch_file(row['local_file'] + ".failed")
                        else:
                            print(com)
                        
                                    
    @staticmethod
    def get_corpora_dir(root):
        return(root + "/corpora")

    @staticmethod
    def get_credentials_dir(root):
        return(root + "/credentials")

    @staticmethod
    def get_corpus_manifest(root, corpus_set):
        return(Corpus.get_corpora_dir(root) + "/" + corpus_set + "/" + "MANIFEST")

    @staticmethod
    def update_credential(root, set_name, credential):
        print("Updating credential for set {} with string {}".format(set_name, credential))
        cred_file = Corpus.get_corpora_dir(root) + "/" + set_name + "/credentials.json"
        cur_cred = load_json(cred_file)
        new_cred = parse_json(credential)
        if (not cur_cred['corpus'] == new_cred['corpus']):
            err_quit("Credential add failed. Corpora do not match. Set Name {} != new credential set name {}".format(cur_cred['corpus'], new_cred['corpus']))
        write_update = False
        if ("urls" in new_cred.keys()):
            for url in new_cred['urls'].keys():
                for attr in ['type', 'user', 'password']:
                    if (not attr in new_cred['urls'][url].keys()):
                        err_quit("new credential for URL {} is missing a {} attribute".format(url, attr))
                    if (url in cur_cred['urls'].keys()):
                        if (not cur_cred['urls'][url][attr] == new_cred['urls'][url][attr]):
                            write_update = True
                    else:
                        write_update = True
                if (write_update): ## As so as one triggers, do the update
                    cur_cred['urls'][url] = new_cred['urls'][url]
                    cur_cred['urls'].pop('Initial credential file. Use --add_credentail with NIST provided string', None)
            if (write_update):
                print("   Updating file")
                write_backup(cred_file)
                write_json(cred_file, cur_cred)
            else:
                print("   Nothing to update")
        else:
            err_quit("No urls found in new credentials")

    def is_valid(self):
        return(self.valid)

    def get_message(self):
        return(self.message)

    def dump_info(self):
        print("Corpus Info:")
        print("   Name: {}".format(self.set_name))
        #print("   isValid: {}".format(self.valid))
        if self.valid:
            print("   Location: {}".format(self.set_corpus_dir))
            print("   Credentials: {}".format(self.credentials))
            print("   Manifest File: {}".format(self.set_manifest_file))
            print("      Num Files: {} - {} missing download credentials".format(len(self.manifest.FileID), 
                                                                                  len(self.manifest.file_credentialed[self.manifest.file_credentialed == False])))
            #print("          Num Files with file Download: {}".
            #      format(len(self.manifest.URL_Type[self.manifest.URL_Type == 'file'])))
            #print("          Num FilesVideos with archive Download: {}".
            #      format(len(self.manifest.URL_Type[self.manifest.URL_Type == 'archive'])))
            print("      Download Status:")
            print("          Files downloaded: {} files, {} bytes, {:.2f} GB".
                  format(len(self.manifest.URL_Type[self.manifest.file_present == True]), 
                         self.manifest.FileByteSize[self.manifest.file_present == True].sum(), 
                         self.manifest.FileByteSize[self.manifest.file_present == True].sum() / 1000000000))
            print("          Credentialed files NOT downloaded: {} files, {} bytes, {:.2f} GB".
                  format(len(self.manifest.URL_Type[(self.manifest.file_present == False) & (self.manifest.file_credentialed == True)]),
                         self.manifest.FileByteSize[(self.manifest.file_present == False) & (self.manifest.file_credentialed == True)].sum(), 
                         self.manifest.FileByteSize[(self.manifest.file_present == False) & (self.manifest.file_credentialed == True)].sum() / 1000000000))
            print("          Non-Credentialed files NOT downloaded: {} files, {} bytes, {:.2f} GB".
                  format(len(self.manifest.URL_Type[(self.manifest.file_present == False) & (self.manifest.file_credentialed == False)]),
                         self.manifest.FileByteSize[(self.manifest.file_present == False) & (self.manifest.file_credentialed == False)].sum(), 
                         self.manifest.FileByteSize[(self.manifest.file_present == False) & (self.manifest.file_credentialed == False)].sum() / 1000000000))
            
