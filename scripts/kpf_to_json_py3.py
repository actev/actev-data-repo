#!/usr/bin/env python2

import os
import sys
import errno
import yaml
import json
import glob

from functools import reduce, partial
from operator import add

from multiprocessing import Pool, cpu_count

from multiprocessing.pool import ThreadPool

try:
    from yaml import CLoader as Loader, CDumper as Dumper
except ImportError:
    from yaml import Loader, Dumper

import argparse, logging

logger=logging.getLogger(__file__)
handler = logging.StreamHandler(sys.stdout)
handler.setFormatter(logging.Formatter('[%(asctime)s] {}-%(levelname)s: %(message)s'.format(__file__)))
logging.getLogger().addHandler(handler)

lib_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), "lib")
sys.path.append(lib_path)

from sparse_signal import SparseSignal as S

def err_quit(msg, exit_status=1):
    logger.exception("[Error] {}".format(msg))
    exit(exit_status)

def mkdir_p(path):
    try:
        os.makedirs(path)
    except OSError as exc:
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else:
            err_quit("{}. Aborting!".format(exc))

def yield_file_to_function(file_path, function, mode = 'w'):
    try:
        with open(file_path, mode) as out_f:
            return function(out_f)
    except IOError as ioerr:
        err_quit("{}. Aborting!".format(ioerr))

def load_yaml_file(file_path):
    try:
        return yield_file_to_function(file_path, lambda f: yaml.load(f, Loader=Loader), mode = 'r')
    except yaml.YAMLError as yaml_err:
        err_quit(yaml_err)

def load_txt_list(file_path):
    return list(yield_file_to_function(file_path, lambda f: map(lambda s: s.rstrip(), f.readlines()), mode = 'r'))

def load_csv(file_path, sep=','):
    return yield_file_to_function(file_path, lambda f: list(map(lambda s: tuple(s.rstrip().split(sep)), f.readlines())), mode = 'r')

def load_json(json_fn):
    try:
        with open(json_fn, 'r') as json_f:
            return json.load(json_f)
    except IOError as ioerr:
        err_quit("{}. Aborting!".format(ioerr))

def rect_to_ss(geom_str):
    x1, y1, x2, y2 = list(map(int, geom_str.split(" ")))

    return S({x1: S({y1: 1, y2 + 1: 0}), x2 + 1: 0})

def rect_to_bbox(geom_str):
    x1, y1, x2, y2 = list(map(lambda x: int(float(x)), geom_str.split(" ")))

    return { "boundingBox": { "x": x1, "w": x2 - x1, "y": y1, "h": y2 - y1 } }

def index_frames_at_n(n):
    def index_frames(frame):
        return frame + n

    return index_frames

def get_max_dict(x, y):
    if len(x) > len(y):
        return x
    return y

def build_tracks(frame_indexer, geom_records):

    raw_tracks = {}
    for rec in geom_records:
        geom_rec = rec["geom"]
        key = geom_rec["id1"]

        frame = geom_rec["ts0"]

        bbox = rect_to_bbox(geom_rec["g0"])

        h = raw_tracks.get(key, {})
        raw_tracks[key] = h
        h[frame_indexer(frame)] = bbox

    
    raw_tracks_reduced = {}
    for rec in raw_tracks.items():
        k, v = rec
        raw_tracks_reduced[k] = S(v).join(S(), get_max_dict, {})

    return raw_tracks_reduced


def build_actor_type_lookup(type_records):
    actor_type_lookup = {}
    for rec in type_records:
        type_rec = rec["types"]

        key = type_rec["id1"]
        t = list(type_rec["cset3"].keys())[0]

        actor_type_lookup[key] = t

    return (actor_type_lookup)

def build_timespan_to_signal(frame_indexer):
    def _ts_to_sig(ts):
        s, e = ts["tsr0"]
        return S({frame_indexer(s): 1, frame_indexer(e) + 1: 0})

    return _ts_to_sig

def _temporal_spatial_joiner(a, b):
    return b if a == 1 else {}

def build_actor_mapper(type_lookup, tracks, filename, ts_to_sig_func, obj_id_gen):
    def actor_mapper(rec):
        key = rec["id1"]
        typ = type_lookup[key]

        t_signal = reduce(add, list(map(ts_to_sig_func, rec["timespan"])), S())

        selected_signal = t_signal.join(tracks.get(key, S()), _temporal_spatial_joiner, {})

        return { "objectType": typ,
                 "objectID": next(obj_id_gen),
                 "localization": { filename: selected_signal } }

    return actor_mapper

def build_activity_mapper(frame_indexe, type_lookup, tracks, filename, act_id_gen, activity_type_map = None):
    ts_to_sig = build_timespan_to_signal(frame_indexer)

    def activity_mapper(rec):
        act_rec = rec["act"]    
        act_id = next(act_id_gen)
        if activity_type_map:
            act_type=activity_type_map[list(act_rec["act2"].keys())[0]]
        else:
            act_type = list(act_rec["act2"].keys())[0]
        localization = reduce(add, map(ts_to_sig, act_rec["timespan"]), S())

        obj_id_gen = build_id_generator(1)
        actor_mapper = build_actor_mapper(type_lookup, tracks, filename, ts_to_sig, obj_id_gen)

        actors = list(map(actor_mapper, act_rec.get("actors", [])))

        return { "activity": act_type,
                 "activityID": act_id,
                 "localization": { filename: localization },
                 "objects": actors }

    return activity_mapper

def build_activity_index(activities, object_types = None, object_type_map = None, activity_type_map = None):
    def props(a):
        h = {}
        if object_types:
            h["objectTypes"] = object_types

        if object_type_map:
            h["objectTypeMap"] = object_type_map

        return h
    if activity_type_map:
        act=[]
        for key, value in activity_type_map.items():
            act.append(value)
        uniq_act=list(set(act))
        return { a: props(a) for a in uniq_act }
    else:
        return { a: props(a) for a in activities }

def build_id_generator(initial = 1):
    while True:
        yield initial
        initial += 1

def write_reference(out_dir, reference, prefix = None):
    mkdir_p(out_dir)

    def _f(outfile):
        outfile.write(json.dumps(reference, indent=2, sort_keys=True))

    if prefix:
        out_fn = "{}/{}.activities.json".format(out_dir, prefix)
    else:
        out_fn = "{}/activities.json".format(out_dir)

    yield_file_to_function(out_fn, _f)

def write_activity_index_json(out_dir, activities):
    mkdir_p(out_dir)

    def _f(outfile):
        outfile.write(json.dumps(activities, indent=2, sort_keys=True))

    yield_file_to_function("{}/activity-index.json".format(out_dir), _f)

def build_file_index_json_mapper(file_md_lookup):
    def mapper(fn):
        md = file_md_lookup[fn]
        num_frames = md["num_frames"]
        framerate = md["framerate"]

        return { "framerate": framerate, "selected": S({1: 1, num_frames + 1: 0}) }

    return mapper


def find_file_in_dir(filename, directory, strict=True):
    filepaths = [os.path.join(root_dir, path) for root_dir, _, filenames in os.walk(directory) for path in filenames if os.path.basename(path) == filename ]
    if filepaths:
        return filepaths[0]
    if strict:
        raise Exception("Cannot find {} under {}".format(filename, directory))
    return None

# The activity ID generator will be a global variable
act_id_gen = build_id_generator(1)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="KPF annotation conversion script")

    parser.add_argument("-a", "--activity-index", help="List of activities to include", required=True)
    parser.add_argument("-p", "--activity-type-map", help="Activity mapping file")
    parser.add_argument("-j", "--object-index", help="List of objects to include (in the activity-index)")
    parser.add_argument("-t", "--object-type-map", help="Object type remapping (in the activity-index)")
    parser.add_argument("-f", "--file-index", help="JSON file index that contains the files metadata ")
    parser.add_argument("-d", "--data-dir", help="Directory containing KPF files referenced in the provided file index", required=True)

    parser.add_argument("-i", "--dump-individual", help="Enable dumping of individual file references", action="store_true")
    parser.add_argument("--ignore-missing-annotations", help="Ignore missing annotations", action="store_true")
    parser.add_argument("-v", "--verbose", help="Set logging level to debug", action="store_true")

    parser.add_argument("-o", "--out-dir", help="Directory for output files", required=True)

    args = parser.parse_args()
    
    logging.getLogger().setLevel(logging.INFO if args.verbose else logging.DEBUG)
    
    # Offset by 1 as we're indexing frames at 1 rather than 0
    frame_indexer = index_frames_at_n(1)

    reference_file_names = json.load(open(args.file_index, 'r')).keys()

    def filename_formatter(fn):
        '''Function to map a filename from the kpffilelist to a filename under file-metadata
        '''
        return "{}".format(fn)

    selected_activities = load_txt_list(args.activity_index)

    if args.object_index:
        selected_objects = load_txt_list(args.object_index)
    else:
        selected_objects = None

    if args.activity_type_map:
        activity_type_map = { k: v for k, v in load_csv(args.activity_type_map) }
    else:
        activity_type_map = None
    if args.object_type_map:
        object_type_map = { k: v for k, v in load_csv(args.object_type_map) }
    else:
        object_type_map = None

    def activity_filter(act):
        
        return "act" in act and list(act["act"]["act2"].keys())[0] in selected_activities


    def file_activities_reducer(video_filename, ignore_missing_annotations):

        video_filename_noext = os.path.splitext(video_filename)[0]
        # Finding reference files paths
        geom_fn = find_file_in_dir(video_filename_noext + '.geom.yml', args.data_dir, not ignore_missing_annotations)
        activities_fn = find_file_in_dir(video_filename_noext + '.activities.yml', args.data_dir, not ignore_missing_annotations)
        types_fn = find_file_in_dir(video_filename_noext + '.types.yml', args.data_dir, not ignore_missing_annotations)

        # Loading reference files
        if not all([geom_fn, activities_fn, types_fn]):
            logger.warning('One of the reference files for {} was not found. Skipping.'.format(video_filename_noext))
            return []

        logger.info("Processing reference files for video '{}'".format(video_filename))

        types = filter(lambda x: "types" in x, load_yaml_file(types_fn))
        geom_recs = filter(lambda x: "geom" in x, load_yaml_file(geom_fn))
        activities = filter(activity_filter, load_yaml_file(activities_fn))

        type_lookup = build_actor_type_lookup(types)
        tracks_lookup = build_tracks(frame_indexer, geom_recs)

        out_activities = list(map(build_activity_mapper(frame_indexer, type_lookup, tracks_lookup, filename_formatter(video_filename), act_id_gen, activity_type_map), activities))

        if args.dump_individual:
            file_reference = { "filesProcessed": list(map(filename_formatter, [ video_filename ])),
                               "activities": out_activities }

            write_reference(args.out_dir, file_reference, video_filename_noext)

        return out_activities

    activities_lists = []
    partial_file_activities_reducer = partial(file_activities_reducer, ignore_missing_annotations=args.ignore_missing_annotations)
    with ThreadPool(cpu_count()) as p:
        activities_lists = p.map(partial_file_activities_reducer, reference_file_names )

    activities = []
    for activities_list in activities_lists:
        activities.extend(activities_list)

    combined_reference = { "filesProcessed": list(map(filename_formatter, reference_file_names)),
                           "activities": activities }

    write_reference(args.out_dir, combined_reference)

    write_activity_index_json(args.out_dir, build_activity_index(selected_activities, selected_objects, object_type_map, activity_type_map))
