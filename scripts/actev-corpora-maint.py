#!/usr/bin/env python3

# actev-corpus-maint.py
# Author(s): Jon Fiscus

# This software was developed by employees of the National Institute of
# Standards and Technology (NIST), an agency of the Federal
# Government. Pursuant to title 17 United States Code Section 105, works
# of NIST employees are not subject to copyright protection in the
# United States and are considered to be in the public
# domain. Permission to freely use, copy, modify, and distribute this
# software and its documentation without fee is hereby granted, provided
# that this notice and disclaimer of warranty appears in all copies.

# THE SOFTWARE IS PROVIDED 'AS IS' WITHOUT ANY WARRANTY OF ANY KIND,
# EITHER EXPRESSED, IMPLIED, OR STATUTORY, INCLUDING, BUT NOT LIMITED
# TO, ANY WARRANTY THAT THE SOFTWARE WILL CONFORM TO SPECIFICATIONS, ANY
# IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
# PURPOSE, AND FREEDOM FROM INFRINGEMENT, AND ANY WARRANTY THAT THE
# DOCUMENTATION WILL CONFORM TO THE SOFTWARE, OR ANY WARRANTY THAT THE
# SOFTWARE WILL BE ERROR FREE. IN NO EVENT SHALL NIST BE LIABLE FOR ANY
# DAMAGES, INCLUDING, BUT NOT LIMITED TO, DIRECT, INDIRECT, SPECIAL OR
# CONSEQUENTIAL DAMAGES, ARISING OUT OF, RESULTING FROM, OR IN ANY WAY
# CONNECTED WITH THIS SOFTWARE, WHETHER OR NOT BASED UPON WARRANTY,
# CONTRACT, TORT, OR OTHERWISE, WHETHER OR NOT INJURY WAS SUSTAINED BY
# PERSONS OR PROPERTY OR OTHERWISE, AND WHETHER OR NOT LOSS WAS
# SUSTAINED FROM, OR AROSE OUT OF THE RESULTS OF, OR USE OF, THE
# SOFTWARE OR SERVICES PROVIDED HEREUNDER.

# Distributions of NIST software should also include copyright and
# licensing statements of any third-party software that are legally
# bundled with the code in compliance with the conditions of those
# licenses.

import argparse
import sys
import os

from actev_corpora_maint.utils import *
from actev_corpora_maint.corpus import Corpus
from actev_corpora_maint.partition import Partition


def survey_corpora(root, select_sets, transfer_tool, aws_transfer_tool, credential):
    corpora_dir = Corpus.get_corpora_dir(root)

    if (not dir_exists(corpora_dir)):
        err_quit("Corpora dir {} does not exist".format(corpora_dir))
    
    sets = []
    for f in os.listdir(corpora_dir):
        if (len(select_sets) == 0 or (f in select_sets)):
            if (not credential == ""):
                Corpus.update_credential(root, f, credential)
            ds = Corpus(root, f, transfer_tool, aws_transfer_tool)
            if ds.is_valid():
                sets.append(ds) 
            else:
                warn("Directory {} in {} is not valid".format(f, corpora_dir))
                warn("  Message: {}".format(ds.get_message()))
    return(sets)

def survey_partitions(root, select_partitions):
    partitions_dir = Partition.get_partitions_dir(root)

    if (not dir_exists(partitions_dir)):
        err_quit("Partitions dir {} does not exist".format(partitions_dir))

    partition_files = []
    if len(select_partitions) != 0:
        for f in os.listdir(partitions_dir):
            if f in select_partitions:
                part = Partition(root, f)
                if part.is_valid():
                    for video_file in part.files:
                        if not video_file in partition_files:
                            partition_files.append(video_file)
                else:
                    warn("Directory {} in {} is not valid".format(f, partitions_dir))
                    warn("  Message: {}".format(part.get_message()))
    return(partition_files)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Video Data Downloader for ActEV Evaluations")

    parser.add_argument("--operation", type=str, nargs='?', help='The operation to perform',
                        choices=['summary', 'download', 'download-dry-run', 'manifest'], default="summary")
    parser.add_argument("--transfer_tool", type=str, nargs='?', help='The transfer utility to use', choices=['curl', 'wget'], default="curl")
    parser.add_argument("--aws_transfer_tool", type=str, nargs='?', help='The transfer utility to use for AWS Buckets', choices=['aws', 's3cmd'], default="aws")
    parser.add_argument("--root_dir", type=str, nargs=1, help='The directory name of the Repo.', 
                        default=os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
    parser.add_argument("--corpus", type=str, nargs="*", help='Limit the corpora to the list', 
                        default=[])
    parser.add_argument("--add_credential", type=str, nargs="?", help='Adds the credentials to the collection', 
                       default="")
    parser.add_argument("--partitions", type=str, nargs="*", help='Limit the downloaded corpora to files in a partition', 
                        default=[])
    parser.add_argument("--regex", type=str, nargs="*", help='Limit the download files to those that match the provided Regular Expression. The RE must match the entire string. e.g., ".*string.*"', 
                        default=[])
    
    args = parser.parse_args()

    if ((not args.add_credential == "") and len(args.corpus) != 1):
        err_quit("Option --add_credential requires one set to be defined via --corpus")


    if (args.aws_transfer_tool == 's3cmd'):
        import boto3

    sets = survey_corpora(args.root_dir, args.corpus, args.transfer_tool, args.aws_transfer_tool, args.add_credential)
    partition_files = survey_partitions(args.root_dir, args.partitions)
    for set in sets:
        if args.operation == "summary":
            set.dump_info()
        elif args.operation == "download":
            set.load_corpus(False, partition_files, args.regex)
        elif args.operation == "download-dryrun":
            set.load_corpus(True, partition_files, args.regex)
        elif args.operation == "manifest":
            print(set.manifest.head())
