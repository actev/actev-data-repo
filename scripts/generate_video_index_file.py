import argparse, json, logging, os

import datetime

import pandas

# Append the camera_type field to an existing file index.

# Usage:
#   python scripts/update_video_index_file.py -f ./partitions/ActEV19-MEVA-Test2-20190613/file-index.json -vt ./recording-metadata/m1/m1.camera_status.txt -v

def get_camera_type_map_from_file(camera_metadata_path):
    '''From a file that maps video types to camera ids,
    Return a dict of camera ids {"camera_id":camera_type, ... }
    '''
    camera_type_map_df = pandas.read_csv(camera_metadata_path, sep='|')
    camera_type_map_df = camera_type_map_df[['CameraID', 'EOIR']].drop_duplicates()
    camera_type_map_dict = camera_type_map_df.set_index('CameraID').to_dict()['EOIR']

    logging.debug('Camera types {}'.format(camera_type_map_dict))

    return camera_type_map_dict


def find_videos_paths_map(video_filepaths_path, video_filenames):
    result = {}

    for filepath in open(video_filepaths_path).readlines():
        for video_filename in video_filenames:
            if video_filename in filepath:
                result[video_filename] = filepath.strip()
                break

    return result


def get_video_metadata_from_video_md_file(video_metadata_path, camera_metadata_path, video_filepaths_path, filter_filenames):
    '''Columns in the file:
    ScenarioRecordingID|ClipID|ClipFileName|CameraID|LocationName|DateTime|Date|BeginTime|EndTime|FrameCount
    Should turn into the following:
        "camera_id"
        "begin_time"
        "end_time"
        "datetime"
        "date"
        "filename"
        "framerate"

    '''

    video_metadata_df = pandas.read_csv(video_metadata_path, sep='|').set_index('ClipFileName')

    # Keep some columns only
    video_metadata_df = video_metadata_df[['CameraID', 'BeginTime', 'EndTime', 'Date', 'FrameCount']]

    # Filter certain filenames if any
    if filter_filenames:
        video_metadata_df = video_metadata_df.loc[set(filter_filenames)]

    # Calculate the framerate
    video_metadata_df['BeginTime_t'] = pandas.to_datetime(video_metadata_df['BeginTime'], format='%H-%M-%S')
    video_metadata_df['EndTime_t'] = pandas.to_datetime(video_metadata_df['EndTime'], format='%H-%M-%S')
    video_metadata_df['framerate'] = video_metadata_df.apply(lambda row: int(row.FrameCount / (row.EndTime_t - row.BeginTime_t).total_seconds()), axis=1)

    # Rename the columns
    rename_dict={
        'CameraID': 'camera_id',
        'BeginTime': 'begin_time',
        'EndTime': 'end_time',
        'Date': 'date',
        'FrameCount': 'frame_count',
        'framerate': 'framerate'
    }
    video_metadata_dict = video_metadata_df.rename(columns=rename_dict)

    # Convert to dict
    video_metadata_dict = video_metadata_dict[rename_dict.values()].to_dict('index')

    # Get the camera_types
    logging.info('Loading camera metadata')
    camera_types_map = get_camera_type_map_from_file(camera_metadata_path)
    
    # Get the video paths
    logging.info('Loading videos filepaths')
    video_paths_map = find_videos_paths_map(video_filepaths_path, filter_filenames)

    logging.info('Parsing file-index content')
    # Update the dict with a camera type field
    # And add a 'selected' key in each sub-dict:
    new_dict = {}
    for video_filename, md_dict in video_metadata_dict.items():
        new_dict[video_filename] = video_metadata_dict[video_filename]

        new_dict[video_filename]["camera_type"] = camera_types_map[new_dict[video_filename]['camera_id']]

        if video_filename in video_paths_map:
            new_dict[video_filename]["filename"] = video_paths_map[video_filename]
        else:
            new_dict[video_filename]["filename"] = 'MISSING'
            logging.error('No path found for file {}'.format(video_filename))

        
        new_dict[video_filename]["selected"] = {
                                            "1": 1,
                                            str(video_metadata_dict[video_filename]['frame_count'] + 1): 0
                                            }
        del(new_dict[video_filename]['frame_count'])

    return video_metadata_dict


def create_index_file(files_list_path, camera_metadata_path, video_metadata_path, video_filepaths_path, output_file):
    '''Replace each index file with an updated copy:
    Original index-file.json:
        {'video_filename': {'framerate':30, ...}}
    New index-file.json:
        {'video_filename': {'camera_type': ..., framerate':30, ...}}

    Then, moves the original file to index-file.json.old and
    overwrites the existing index file with newer entries.
    '''

    file_index_content = {}
    logging.info('Loading file {}'.format(files_list_path))

    with open(files_list_path, 'r') as file_list_cursor:

        video_file_names = file_list_cursor.readlines()
        video_file_names = [ video_filename.strip() for video_filename in video_file_names ]

        video_metadata_dict = get_video_metadata_from_video_md_file(video_metadata_path,
                                                                    camera_metadata_path,
                                                                    video_filepaths_path,
                                                                    filter_filenames=video_file_names)


        for video_filename in video_file_names:
            if video_filename in video_metadata_dict:
                file_index_content[video_filename] = video_metadata_dict[video_filename]
            else:
                logging.error('No metadata found for video {}'.format(video_filename))

    with open(output_file, 'w') as file_index_cursor:
        logging.info('{} created.'.format(output_file))
        json.dump(file_index_content, file_index_cursor, indent=4, sort_keys=True)


def cli_parser():
    parser = argparse.ArgumentParser(description="Generate a file index")

    parser.add_argument("-f", "--files-list-path", help="Plain text containing a list of filenames to generate an index file for", required=True)
    parser.add_argument("-cm", "--camera-metadata-path", help="Document mapping cameras and camera types", required=True)
    parser.add_argument("-vm", "--video-metadata-path", help="Document mapping videos and metadata", required=True)
    parser.add_argument("-vp", "--video-filepaths-path", help="Document listing videos paths", required=True)
    parser.add_argument("-o", "--output-file", help="Output JSON file", required=True)
    parser.add_argument("-v", "--verbose", help="Turn debug mode on", action='store_true')
    parser.set_defaults(func=create_index_file)

    args = parser.parse_args()

    logging.basicConfig()


    if hasattr(args, 'func') and args.func:
        
        logging.getLogger().setLevel(logging.INFO)
        if args.verbose:
            logging.getLogger().setLevel(logging.DEBUG)

        args_dict = dict(args.__dict__)
        del args_dict['func']
        del args_dict['verbose']

        args.func(**args_dict)
    else:
        parser.print_help()


if __name__ == '__main__':
    cli_parser()