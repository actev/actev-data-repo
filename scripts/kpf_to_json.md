This document describes how to generate a set of JSON formatted input/index files for the DIVA and ActEV evaluations. Source dataset files are expected to be in Kitware's KPF format.

## Usage:
```
kpf_to_json_avi.py -f kpffilelist.txt -d annot/ -o outputdir -i -a activity.txt -p activitymapping.txt -m file-index.json -j objind.txt -t objectmap.txt
```

## Input files
Prepare input files for 'kpf_to_json.py':

  * a filelist.txt (newline separated list of filenames (without extensions)) – This is the list of files that will be included in the output dataset
  * an activity-index.txt (newline separated list of activities) – This is the list of activities included in the output dataset
  * an object-index.txt (newline separated list of objects) – This is the list of objects included in the output dataset
  * a file-metadata.json file (JSON file including the framerate and number of frames for each file in the source dataset)
  * an objecttypemap.csv file (comma-separated file with source object type, and destination object type to be included in the "objectTypeMap" for every activity)
  * an activitymapping.txt, that maps the activities in the KPF files to the names in our activity list
  * an annot/ directory containing the kitware annotation files

### 1.1 filelist.txt

```
$ head VIRAT-V1_test-file-index.txt
VIRAT_S_000200_04_000937_001443.mp4
VIRAT_S_000200_06_001693_001824.mp4
VIRAT_S_000202_01_001334_001520.mp4
VIRAT_S_000202_05_001768_001849.mp4
VIRAT_S_000203_00_000128_000160.mp4
VIRAT_S_000203_02_000357_000443.mp4
VIRAT_S_000204_01_000119_000219.mp4
VIRAT_S_000204_06_001527_001560.mp4
VIRAT_S_000204_08_001704_001746.mp4
```



### 1.2 activity-index.txt

```
$head VIRAT-V1_leaderboard-activity-index.txt
Closing
Closing_Trunk
Entering
Exiting
Interacts
Loading
Open_Trunk
Opening
Pull
Riding
```


### 1.3 object-index.txt

```
$head VIRAT-V1_EVAL-object-index.txt
Construction_Vehicle
Person
Vehicle
```


### 1.4 file-metadata.json

```
$head VIRAT-V1_file-metadata.json
{
  "VIRAT_S_000003.mp4": {
    "framerate": 30.0,
    "num_frames": 20940
  },
  "VIRAT_S_000200_04_000937_001443.mp4": {
    "framerate": 30.0,
    "num_frames": 15164
  },
  "VIRAT_S_000200_06_001693_001824.mp4": {
```


### 1.5 objecttypemap.csv

```
$head VIRAT-V1_EVAL-objecttypemap.csv
Construction_Vehicle,*Vehicle*
Vehicle,*Vehicle*
```

### 1.6 activitymapping.txt

**First column:** Kitware's activity IDs,
**Second column:** NIST's activity id,
```
$head activitymapping.txt
Close_Trunk,Closing_Trunk
Close_Vehicle_Door,person_closes_vehicle_door
Embrace_Interaction,person_person_embrace
Enter_Facility,person_enters_through_structure
Exit_Facility,person_exits_through_structure

```

Author: Joy

Created: 2018-07-09 Mon 12:22
