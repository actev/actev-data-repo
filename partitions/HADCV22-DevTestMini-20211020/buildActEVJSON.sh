#!/bin/sh

prereq(){
    file=$1
    mesg=$2
    if [ ! -f $file ] ; then err_exit "Error: $file dos not exist. $mesg" ; fi
}

err_exit(){ 
    echo Error: $@  Aborting
    exit 1
}

######## Begin

for needs in files-list.txt files-list.annotated.txt videos_paths.txt  ../../recording-metadata/m1/m1.camera_daily_status.txt ../../recording-metadata/m1/m1.camera_intrinsics.txt  ../../recording-metadata/m1/m1.clip.txt ../../recording-metadata/m1/m1.clip_to_slot.txt ../../recording-metadata/m1/m1.clip_to_slot.txt ; do
    if [ ! -f $need ] ; then err_exit "Error: $need is required" ; fi
done
cat files-list.txt | sed 's:^:./:' > videos_paths.txt

echo "Build file-index.scoring.json.  NOTE: The warnings of missing files is expected"

python3 ../../scripts/generate_video_index_file.py -sd  ../../recording-metadata/m1/m1.clip_to_slot.txt \
	-f files-list.txt -cm ../../recording-metadata/m1/m1.camera_daily_status.txt -ci ../../recording-metadata/m1/m1.camera_intrinsics.txt -vm ../../recording-metadata/m1/m1.clip.txt -vp videos_paths.txt \
        -o file-index.json -sd ../../recording-metadata/m1/m1.clip_to_slot.txt -cs ../../recording-metadata/m1/m1.camera_set.txt \
	-sy ../../recording-metadata/m1/m1.clip_sync_table.txt 2>&1 | tee Log.step1.txt

if [ ! -f file-index.json ] ; then err_exit "File file-index.json not generated"; fi

echo "Starting Step 2: Building activities.json and activity-index.json"

if [ ! -f activity.txt ] ; then err_exit "activity.txt: is required" ; fi
if [ ! -f activitymapping.txt ] ; then err_exit "activitymapping.txt: is required" ; fi
if [ ! -f objind.txt ] ; then err_exit "objind.txt: is required" ; fi
if [ ! -f objectmap.txt ] ; then err_exit "objectmap.txt: is required" ; fi
if [ ! -f file-index.json ] ; then err_exit "file-index.json: is required" ; fi

python3 ../../scripts/kpf_to_json_py3.py -f file-index.json -d ./kitware-data \
	-o . -a activity.txt -p activitymapping.txt -j objind.txt -t objectmap.txt | tee Log.step2.txt

echo ""
echo "Counting Instances: See Log.instancecounts.txt"
grep '"activity"'  activities.json | awk '{print $2}' | sed 's/[,"]//g' | sort | uniq -c | tee Log.instancecounts.txt

