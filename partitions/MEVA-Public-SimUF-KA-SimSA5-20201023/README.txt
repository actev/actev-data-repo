File: partitions/MEVA-Public-SimUF-KA-SimSA5-20201023/README.txt
Date: 20201030

This directory contains three main directories for test sets that support Unknown Facility
and the surprise activity testing.  The directories are:

 - system_input - Contains the files provided to the system during the activity training phase.
 - test_input   - Contains the files provided to the system during execution of the system
                  on test material
 - reference    - Contains the reference files for scoring performance of a system.

Each of the directories is described below:

1. system_input

The system_input directory contains the source material provided to an ActEV system
capable of surprise activity training.  This data is fully usable by the system
without restriction.  The directory  contains the following:


<sin>/activity-index.json - The main JSON file for defining activities per <REF>.  
       The file enumerates the activites, their types, and for the surpeise activities 
       the training exemplars.  The file contains both known and surprise activity
       entries. File names are relative to the directory containing this file.

<sin>/training - the surprise activity training resources

<sin>/training/<SA1>     - the directory with exemplars for surprise activity e.g., "sa001"

<sin>/training/<SA1>/<EXEMPLARID> - the directory for exemplars e.g., sa001_ex001

     - activities.json   - the activity instance JSON describing the instance
     - file-index.json   - the file JSON for only this instance.

2. test_input

The test_input directory contains the datafiles necessary for executing a system on the
test material.

<tes>/activity-index.json - the activity index without the training dictionary.  The system is
        expected to have already been trained on all activities

<tes>/file-index.json     - the file index that contains the entire extent of the video, i.e.,
        the training instances have not been labeled as no-score zones.


3. reference

This directory contains the reference directory for scoring systems
capable of surprise activity training.  This data SHOULD NOT be shared with researchers.
The directory contains the following:

<dir>/activity-index.json - The main JSON file for defining activities per <REF>.
       The file enumerates the activites, their types, but no training dictionaries.

<dir>/file-index.json - the file index JSON with no-score regions indicated by gaps
       in the scored signal.

<dir>/activities.json - The activity instances for the activities.  This contains the evaluated
       instances and excludes the training instances.



