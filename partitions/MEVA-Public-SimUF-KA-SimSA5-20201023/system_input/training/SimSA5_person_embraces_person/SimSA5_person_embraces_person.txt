SimSA5_person_embraces_person

Description:  A physical interaction between two or more people in which they put their arm or arms around the neck, back or waist of one another.  This activity explicitly excludes fighting.

Examples:  two-person hug, group hug, side hug

Start:  This event begins 1 s before physical contact.

End:  This event ends 1 s after physical contact ends.
