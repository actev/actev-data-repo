SimSA5_person_purchases

Description:  A person purchasing something (e.g., coffee, snacks, bus tickets) using cash, credit card, apple/phone pay, etc.  The only track involved in this activity is the purchaser (there may be more than one purchaser for a single transaction).  The purchasing activity must be visible from the camera point of view to be annotated.

Start:  Annotation begins 1 second before the person begins to make the payment transaction. Payment transaction begins when the purchaser’s arm motion begins to present the form of payment.

End:  Annotation ends 1 second after the person receives the purchased item(s) or their return of payment (e.g., credit card, change), whichever occurs later.

The start and end actions may be reversed; all transferring must be complete, but the purchaser can receive their goods before they present payment. In this situation, the start and end criteria would be reversed
