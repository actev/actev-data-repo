SimSA5_vehicle_drops_off_person

Description:  A vehicle drops off a person or group of people.  This encompasses the act of the vehicle stopping (optional), people getting out of the vehicle, and the vehicle driving away (optional).  At least one of the optional vehicle motions must occur and the driver cannot leave the vehicle unattended.  Both vehicle and person tracks should be included in this activity.  Not all tracks will have the same length.

Start:  This event begins 1 s before the vehicle comes to a complete stop or 1 second before the first person begins exiting the vehicle, whichever comes first. People exiting the vehicle should be tracked starting as soon as they are first visible.

End:  This event ends 1 s after the vehicle resumes motion or 1 second after the final person has exited the vehicle, whichever occurs last. People who have exited the vehicle should stop being tracked 1 seconds after exiting the vehicle.
