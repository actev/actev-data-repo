SimSA5_person_carries_heavy_object

Description:  A person or multiple people carrying an oversized or heavy object.  This is characterized by the object being large enough (over half the size of the person) or heavy enough (where the person's gait has been substantially modified) to require being tracked separately; therefore, this activity requires tracking at least one person and one object (i.e., Bag or Other). The carried object must break contact with a surface or ground during the activity. If multiple actors are carrying, they may start or stop participating in the activity independently of the lifetime of the overall activity.  Umbrellas (either opened or closed) are not heavy or sufficiently large objects.

Start:   This event begins when the person (or the first person for multiple people) establishes contact with the object or when the person heavy-carrying is first visible.

End:  This event ends after the person (or the final person for multiple people) loses contact with the object or when the person heavy-carrying is no longer visible.
