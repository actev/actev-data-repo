SimSA5_person_sits_down

Description:  A person sitting down -- transitioning from an upright, standing position to a sitting position in which the body weight is supported primarily by the buttocks in contact with the ground or a horizontal object (e.g., chair, bench).

Start:  Annotation begins when the person’s posture begins to go from standing to sitting.

End:  Annotation ends when the person is completely seated.
