SimSA5_person_exits_vehicle

Description:  A person exiting a vehicle.  The two necessary tracks included in this event are (1) the person exiting and (2) the vehicle being exited.  A special case of “exiting” is dismounting a motorized vehicle (e.g., motorcycle, motorized scooter).  The person does not need to be visible for the duration of the activity; however, when the person is visible they will be annotated.  People will be annotated through bus windows when visible.

Start:  The event begins either (1) 1 s before the door moves (even if the person opening the door is not visible) or (2) if there is no door interaction, 1 s before half of the person’s body is outside the vehicle.

End:  The event ends 1 s after the person has exited the vehicle.
