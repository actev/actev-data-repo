SimSA5_person_unloads_vehicle

Description:  An object moving from vehicle to person.  The two necessary tracks included in this activity are (1) the person performing the unloading and (2) the vehicle being unloaded.  Additionally, if the items being loaded meet the criteria of a unique track, then they are included in this activity.

Start:  The event begins 1 s before the cargo begins to move.  If the start of the event is occluded, then it begins when the cargo movement is first visible.

End:  The event ends after the cargo is released.  If the person holding the cargo begins to walk away from the vehicle, the event ends after 1 s of walking.  If the door or trunk is closed on the vehicle, the event ends when the door or trunk is closed.  If both of these things happen, the event ends at the earlier of the two events. The unloading of multiple objects by the same person with a pause of no more than 1 second will be annotated as one continuous activity.
