SimSA5_vehicle_reverses

Description:  A vehicle moving in the reverse direction.  During this activity, no other vehicle activities (i.e., moving, stopping, starting, left turn, right turn) will be annotated.

Start:  Annotation begins when motion is evident.

End:  Annotation ends when motion is no longer evident.
