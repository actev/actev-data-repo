SimSA5_person_opens_facility_door

Description:  A person opening the door to a facility.  The only track required for this activity is a person track.

Special Examples:

 - A person holds the door open for an extended period of time, possibly to allow other individuals through, then the activity ends with the person holding the door open.

 - A person grabbing an already open door does not get included in the activity.

 - A person grabbing a door that has started closing does get this activity.

Start:  The event begins 1 s before the door starts to move or when the person is first visible.

End:  The event ends after the door stops moving or when the tracked person is no longer visible.
