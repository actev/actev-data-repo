SimSA5_person_talks_on_phone

Description:  A person talking on a cell phone where the phone is being held on the side of the head.  This activity should apply to the motion of putting one’s hand up to the side of their head regardless of the presence of a phone in hand.

Start:  Annotation should begin when the hand makes motion toward side of head or when the person is first visible performing the activity.  Person is first visibly performing the activity when they enter the field of view actively talking on a phone or when talking on phone indicated by a hand to the side of the head is first visible.

End:  Annotation should end after the hand leaves the side of the head or when the person leaves the scene.
