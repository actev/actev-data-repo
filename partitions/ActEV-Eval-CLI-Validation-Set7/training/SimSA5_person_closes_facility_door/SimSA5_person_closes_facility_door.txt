SimSA5_person_closes_facility_door

Description:  A person closing the door to a facility.  Doors closing due to the mechanics of the door with no human assistance are not included in this activity.  The only track required for this activity is a person track.

Start:  The event begins 1 s before the door starts to move or when the person is first visible.

End:  The event ends after the door stops moving closed or when the tracked person is no longer visible.
