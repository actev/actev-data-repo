SimSA5_person_texts_on_phone

Description:  A person texting on a cell phone, including both using the phone with thumbs and fingers and video phone calls.  The latter applies to any situation when the phone is in front of the person (as opposed to along the side of the head) and they are using it, including playing games, checking emails, taking pictures, etc.  A person must be in possession of the phone to be involved in the texting activity.

Start:  Annotation should begin when  “texting” is observed or when the person is first visible performing the activity.

End:  Annotation should end after last instance of “texting” is observed or when the person leaves the scene.

Minimum Duration:  2 seconds continuous, uninterrupted texting.  The goal is to avoid quick, sporadic use of phone (e.g., use of phone as a clock or checking messages).
