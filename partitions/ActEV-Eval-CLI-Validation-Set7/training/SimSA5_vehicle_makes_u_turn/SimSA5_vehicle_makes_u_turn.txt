SimSA5_vehicle_makes_u_turn

Description:  A vehicle making a u-turn is defined as a turn of 180 and should give the appearance of a “U”.  U-turns do not contain left and right turns.  A u-turn is a continuous turn in which the vehicle may not stop for more than 10 s.  This event is determined after a reasonable interpretation of the video and may include turns around obstacles, such as concrete islands.  Examples of acceptable and unacceptable can be found on Slide 17 of “Scene Specific Notes and Do Not Annotate Areas”.

Start:  Annotation begins when the vehicle has ceased linear motion.

End:  Annotation ends 1 s after the car has completed u-turn.
