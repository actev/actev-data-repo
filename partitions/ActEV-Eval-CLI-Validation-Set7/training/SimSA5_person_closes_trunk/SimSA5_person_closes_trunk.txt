SimSA5_person_closes_trunk

Description:  A person closing a trunk.  A trunk is defined as a container designed to store non-human cargo on a vehicle (e.g., rear facing trunk, van rear doors, truck bed).  The necessary tracks are person and vehicle.  A special case exception of “close trunk” is remotely closing trunks where the person is not near the vehicle -- in this case, the person track will not be included in the activity.

Start:  The event begins 1 s before the trunk starts to move.

End:  The event ends after the trunk has stopped moving.
