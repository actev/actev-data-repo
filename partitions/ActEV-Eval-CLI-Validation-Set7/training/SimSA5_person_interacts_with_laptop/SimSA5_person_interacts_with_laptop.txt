SimSA5_person_interacts_with_laptop

Description:  A person actively interacting with a laptop.  This may include actions such as opening/closing a laptop, reading a document on a laptop, or typing.  Brief breaks in interactions with laptop during a continuous activity should be included within the same “person-laptop interaction” event.  Only the person interacting with the laptop is tracked. If there are multiple people interacting with the same laptop, those are separate person-laptop-interaction activities.

Minimum Duration:  5 seconds continuous, uninterrupted interaction with laptop.

Start:  Annotation begins 1 second before active interaction or contact with the laptop first occurs.

End: The activity ends either ACTIVELY or PASSIVELY. The activity ends actively once the laptop has been fully closed (note the closing motion is included in the activity). The activity ends passively if the person’s attention drifts away from the laptop for more than five seconds.
