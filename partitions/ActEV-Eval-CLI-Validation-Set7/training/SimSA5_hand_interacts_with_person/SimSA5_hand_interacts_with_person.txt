SimSA5_hand_interacts_with_person

Description:  A physical interaction between two or more people in which their hands come together or one individual’s hand comes into contact with another person.  Examples include handshakes, high-fives, and fist-bumps, and explicitly excludes fighting and embracing.

Examples:  Handshakes, Holding Hands, High Fives, Fist Bumps, Linked Arms, Pat on Back/Shoulder.

Start:  This event begins 1 s before physical contact.

End:  This event ends 1 s after physical contact ends.
