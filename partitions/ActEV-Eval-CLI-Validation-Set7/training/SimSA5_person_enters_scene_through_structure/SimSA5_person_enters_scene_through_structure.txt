SimSA5_person_enters_scene_through_structure

Description:  A person entering the scene (going into the field of view) through a physical structure (e.g., doorway, arch, gateway, subway entrance/exit).  This distinguishes entering through a structure from entering the FOV via (a) an incidental edge-of-image or (b) incidental occlusion, which both arise from the choice of camera viewpoint rather than inherent structures of the scenarios.  Entering is always determined relative to the field of view.  The only track required for this activity is a person track.

Start:  The event begins 1 s before the individual crosses the entry threshold (regardless of door interaction) or when the individual is first visible.

End:  The event ends 1 s after the individual completely crosses the entry threshold (regardless of door interaction) or when the individual is no longer visible.

Special Case 1: In the event of entering through a stairwell (e.g., subway entrance in GDB scenes), the threshold for entering is the top step of the staircase. Therefore, the activity begins when the person is first visible and ends 1 s after the individual completely crosses the top step.  If the individual remains at the top stair for an extended period, then the activity ends after 1 s of standing at/on the threshold.

Special Case 2: The arch entryway in ALB G328 and G331 constitutes a structure.
