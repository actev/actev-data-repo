SimSA5_person_stands_up

Description:  A person standing up -- transitioning from a seated or prone position to an upright, standing position.

Start:  Annotation begins when the person’s posture begins to go from sitting or prone to standing.

End:  Annotation ends when the person is completely standing in an upright position.  If a person begins walking before completely upright, the annotation ends after 1 s of walking.
