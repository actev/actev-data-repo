SimSA5_person_loads_vehicle

Description:  An object moving from person to vehicle.  The two necessary tracks included in this activity are (1) the person performing the loading and (2) the vehicle being loaded.  Additionally, if the items being loaded meet the criteria of a unique track, then they are included in this activity.

Start:  The event begins 1 s before the cargo to be loaded is extended toward the vehicle (i.e., before a person’s posture changes from one of “carrying” to one of “loading”). If the start of the event is occluded, then it begins when the person and cargo are first visible.

End:  The event ends after the cargo is placed into the vehicle and the person-cargo contact is lost.  In the event of occlusion, it ends when the loss of contact is visible.  The loading of multiple objects by the same person with a pause of no more than 1 second will be annotated as one continuous activity.
