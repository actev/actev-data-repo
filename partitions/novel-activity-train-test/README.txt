File: actev-data-repo/partitions/novel-activity-train-test/README.txt
Date: July 30, 2021

1.0 Introduction

This directory contains MEVA Public Data resources repurposed to
provide a small data set to train an activity detection system and
then test its performance.  The data set renames both the activity
names and file names to help debug system training scripts.

To use this resource, you must first download the videos for the data
set using the following corpus maintainance script.  This assumes you
have already set up the credentials per the top-level README.md.

  % python ./scripts/actev-corpora-maint.py --partitions novel-activity-train-test --operation download

There are two directories to use for the testing: 'train' which
contains data to use for training a system and 'test' a directory to
test the trained system.

Note: The 'file-index.json' in this directory uses the original
filenames.  It is NOT provided for system testing.  Rather, it is used
by the corpus maintainance script to download the video.  The
'scripts' directory contains a single bash script, 'make_data.sh',
that was used to build the resources from the 'MEVA-Public-20200228'
partition and the 'meva-data-repo'.  The script is provided for
documentation and does not need to be executed.

2.0 The 'train' Directory

There are 5 items in the 'train' directory.  All 5 have a use in
training.  The items are:

  - video - a directory containing links to the original R13 videos.
    The command for 'actev-corpora-maint.py' above will download the
    data so that the links are live.

  - kpf - a directory of the Kitware KPF adjusted to the new activity
    names and new file names.

  - activities.json - the activities ActEV Evaluation JSONs for the data set

  - activity-index.json  - the activity ActEV Evaluation JSONs for the data set

  - file-index.json - the file index ActEV Evaluation JSONs for the data set

3.0 The 'test' Directory

The test directory contains the same 5 items as in the 'train'
directory.  However, the activity instance annotations are provided
for scoring, not running the trained system.

4.0 History

July 30, 2021 - Initial version
