#!/bin/sh


base=../../MEVA-Public-20200228
root=natt

cat > activity_map.txt <<EOF
act_hiwp,hand_interacts_with_person
act_pct,person_closes_trunk
act_pcvd,person_closes_vehicle_door
act_pep,person_embraces_person
act_pttp,person_talks_to_person
act_pev,person_exits_vehicle
act_vtl,vehicle_turns_left
act_vpup,vehicle_picks_up_person
act_psd,person_sits_down
act_pests,person_enters_scene_through_structure
EOF



mkdir -p ../train 
mkdir -p ../train/video 
mkdir -p ../train/kpf
mkdir -p ../test
mkdir -p ../test/video 
mkdir -p ../test/kpf


echo Make the PERL filter AND the activity removald filter
echo "perl -pe '" > activity_map.filt.sh
cat activity_map.txt | awk -F, '{print "s/"$2"/"$1"/g;"}' >> activity_map.filt.sh
echo "'" >> activity_map.filt.sh
cat ../../MEVA-Public-20200228/activity-index.json | jq keys | sh activity_map.filt.sh  | grep \" | grep -v act_ | awk -F\" '{print $2}' > activity_map.remove_activity.txt

echo Make the file and camera maps
find  ../../../../meva-data-repo/annotation/DIVA-phase-2/MEVA/kitware | grep types.yml | perl -pe 's:.*/(.*).types.yml:$1:' > /tmp/annotated.txt
cat ../../MEVA-Public-20200228/file-index.json | jq keys | grep avi | sed 's/.avi//' | awk -F\" '{print $2} ' | fgrep -f /tmp/annotated.txt | head -60 | awk -F\" '{if (NR % 2 == 0) { use="test"} else {use="train"} printf("file_%03d,"$1","use"\n",NR)}' > file_map.txt

echo "perl -pe '" > file_map.filt.sh
cat file_map.txt | awk -F, '{print "s/"$2"/"$1"/g;"}' >> file_map.filt.sh
echo "'" >> file_map.filt.sh
echo "perl -pe '" > camera_map.filt.sh
cat file_map.txt | sed 's/.*G/G/' | sort -u | awk -F\" '{printf("s/"$1"/C%03d/g;\n",NR)}' >> camera_map.filt.sh
echo "'" >> camera_map.filt.sh

echo Make the file index
rm -f /tmp/f_orig
for use in test train ; do
    echo '{' > /tmp/x
    for file in `cat file_map.txt | grep $use` ; do
	new=`echo $file|awk -F, '{print $1}'`
	old=`echo $file|awk -F, '{print $2}'`
	cat $base/file-index.json | jq '.[] | select(.clip_id == "'$old'")' | tr '\012' ' ' | perl -pe 's/^/"'$old'.r13.avi":/; s/$/=/; '  >>  /tmp/f_orig 
	cat $base/file-index.json | jq '.[] | select(.clip_id == "'$old'")' | \
	    jq 'del(.camera_set_id)' | jq 'del(.frame_offset_precision_to_reference_clip_id)'|jq 'del(.frame_offset_to_reference_clip_id)' | \
	    jq 'del(.krtd_filename)' | jq 'del(.ply)' | jq 'del(.reference_camera_id)' | jq 'del(.reference_camera_id)'  | jq 'del(.reference_clip_id)' | \
	    jq 'del(.reference_filename)' | jq 'del(.recording_site)' | jq 'del(.slot_date_time)' | \
	    tr '\012' ' ' | awk '{print "\"'$new'.avi\":",$0,","}' >> /tmp/x
    done
    echo '}' >> /tmp/x
    cat /tmp/x | tr '\012' ' ' | perl -pe 's/,\s+\}/\}/' | jq . | sh  file_map.filt.sh | sh camera_map.filt.sh > file-index.$use.mapped.json
    cat file-index.$use.mapped.json | perl -pe 's:"./:"./video/:' > ../$use/file-index.json
done
(echo "{"; cat  /tmp/f_orig  | perl -pe 's/=\s*$//; s/=/,/g' ; echo "}") | jq . > file-index.orig.json
cp file-index.orig.json ../file-index.json 

echo Make the activity_index
echo '{' > /tmp/x
for act in `cat activity_map.txt` ; do
    new=`echo $act|awk -F, '{print $1}'`
    old=`echo $act|awk -F, '{print $2}'`
    cat $base/activity-index.json | jq ".$old" | tr '\012' ' ' | awk '{print "\"'$new'\":",$0,","}' >> /tmp/x
done
echo '}' >> /tmp/x
cat /tmp/x | tr '\012' ' ' | perl -pe 's/,\s+\}/\}/' | jq . > activity-index.json
cp activity-index.json ../train/activity-index.json
cp activity-index.json ../test/activity-index.json

echo Copy the KPFs and make the activities JSONS
cat activity_map.txt  | awk -F, '{print $1}' > activity.txt
cat activity_map.txt  | awk -F, '{print $1","$1}' > activitymapping.txt
for use in train test ; do
    for file in `cat file_map.txt | grep $use` ; do
	new=`echo $file|awk -F, '{print $1}'`
	old=`echo $file|awk -F, '{print $2}'`
	echo $file
	find ../../../../meva-data-repo/| grep $old | grep -v contrib | grep -v nist-kf1-json > /tmp/x
	if [ ! `cat /tmp/x |wc -l` = 3 ] ; then
	    echo Error
	    cat /tmp/x
	fi
	for f in `cat /tmp/x` ; do
	    file=`echo $f|sed 's:.*/::'`
	    newfile=`echo $file | perl -pe 's/.*(\.[^\.]+\.[^\.]+)$/'$new'$1/'`
	    echo $file $newfile
	    cat $f | sh file_map.filt.sh | sh activity_map.filt.sh | fgrep -vf activity_map.remove_activity.txt > ../$use/kpf/$newfile
	done
    done
    echo "Making the ActEV JSONS"
    /Users/jon/homebrew/anaconda3/envs/mypython3/bin/python3 ../../../scripts/kpf_to_json_py3.py -f file-index.$use.json -d ../$use/kpf -o . -a activity.txt -p activitymapping.txt -j objind.txt -t objectmap.txt
    cat activities.json | perl -pe 's/(file_\d\d\d)/$1.avi/' > activities.$use.json
    cp activities.$use.json ../$use/activities.json
done

echo linking in the videos
for use in test train ; do
    for file in `cat file_map.txt | grep $use` ; do
	new=`echo $file|awk -F, '{print $1}'`
	old=`echo $file|awk -F, '{print $2}'`
	echo $new $old
	linkname=`(cd ../$use/video ; find ../../../../corpora/MEVA/drops-123-r13 -type f) | grep $old`
	if [ ! -L ../$use/video/$new.avi ] ; then
	    (cd ../$use/video ; ln -s $linkname $new.avi)
	fi
    done
done

exit

