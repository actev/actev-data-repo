These are annotations for subsets of MEVA according to the DIVA-phase-2 activity definitions.

Subdirectories include:

* KF1-examples: activity samples from the KF (Known Facility) release 1 data. ***Note*** that these annotations are ***not*** clip-complete; they are merely activity exemplars. Other valid activities may exist in the video clips which are not annotated.

